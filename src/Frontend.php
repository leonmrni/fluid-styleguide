<?php
namespace Leonmrni\FluidStyleguide;

/*
 * MIT License
 *
 * Copyright (c) [2017] [Leonmrni]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use Leonmrni\FluidStyleguide\View\TemplateView;

/**
 * @author Leonmrni <developer@leonmrni.com>
 */
class Frontend
{
    /**
     * @var Application
     */
    protected $application;

    /**
     * @var TemplateView $view
     */
    protected $templateView;

    /**
     * @var TemplateView $frameworkView
     */
    protected $frameworkView;

    public function __construct(Application $application)
    {
        $this->application = $application;

        // Initialize views
        $this->templateView = new TemplateView($this->application);
        $this->frameworkView = new TemplateView($this->application);
    }

    public function run()
    {
        $this->setPaths();

        $this->frameworkView->getPaths()->setTemplatePathAndFilename(
            $this->application->getDirectory('applicationRoot') . 'Resources/Private/Templates/Framework/Default.html'
        );
        $this->templateView->getPaths()->setTemplatePathAndFilename(
            $this->application->getDirectory('applicationRoot') . 'Resources/Private/Templates/Page/Default.html'
        );

        $this->templateView->assignMultiple([
            'settings' => $this->application->getConfiguration(),
        ]);

        $this->frameworkView->assign(
            'content',
            $this->templateView->render()
        );
        $this->render($this->frameworkView->render());
    }

    /**
     * Renders the content.
     *
     * @param string $content
     *
     * @return void
     */
    protected function render($content)
    {
        echo trim($content);
    }

    protected function setPaths()
    {
        $this->templateView->getPaths()->setTemplateRootPaths([
            $this->application->getDirectory('applicationRoot') . 'Resources/Private/Templates/Page/',
        ]);
        $this->templateView->getPaths()->setLayoutRootPaths([
            $this->application->getDirectory('applicationRoot') . 'Resources/Private/Layouts/Page/',
        ]);
        $this->templateView->getPaths()->setPartialRootPaths([
            $this->application->getDirectory('applicationRoot') . 'Resources/Private/Partials/Page/',
        ]);
    }
}
