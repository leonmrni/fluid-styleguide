<?php
namespace Leonmrni\FluidStyleguide;

/*
 * MIT License
 *
 * Copyright (c) [2017] [Leonmrni]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use Leonmrni\FluidStyleguide\Service\ConfigurationService;

/**
 * @author Leonmrni <developer@leonmrni.com>
 */
class Application
{
    /**
     * @var array
     */
    protected $directories = [];

    /**
     * @var Frontend
     */
    protected $frontend;

    /**
     * @var array
     */
    protected $configuration = [];

    /**
     * @param string $documentRoot
     *
     * @return Application
     */
    public function __construct($documentRoot)
    {
        $this->directories = [
            'documentRoot' => $documentRoot,
            'applicationRoot' => __DIR__ . '/../',
        ];

        $configurationService = new ConfigurationService(
            $this->directories['applicationRoot'] . 'Configuration/Yaml/'
        );
        $this->configuration = $configurationService->getConfiguration();
        $this->frontend = new Frontend($this);

        return $this;
    }

    /**
     * @param string $directoryName
     *
     * @return string
     */
    public function getDirectory($directoryName)
    {
        return $this->directories[$directoryName];
    }

    /**
     * @return array
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function run()
    {
        $this->frontend->run();
    }
}
