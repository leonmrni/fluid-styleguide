<?php
namespace Leonmrni\FluidStyleguide\View;

/*
 * MIT License
 *
 * Copyright (c) [2017] [Leonmrni]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use Leonmrni\FluidStyleguide\Application;
use TYPO3Fluid\Fluid\Core\Cache\SimpleFileCache;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\View\TemplatePaths;
use TYPO3Fluid\Fluid\View\TemplateView as FluidTemplateView;

/**
 * @author Leonmrni <developer@leonmrni.com>
 */
class TemplateView extends FluidTemplateView
{
    const FLUID_CACHE_DIRECTORY = '/cache/';

    /**
     * @var Application
     */
    protected $application;

    /**
     * @var TemplatePaths $paths
     */
    protected $paths;

    /**
     * Constructor
     *
     * @param Application $application
     * @param null|RenderingContextInterface $context
     */
    public function __construct(Application $application, RenderingContextInterface $context = null)
    {
        parent::__construct($context);

        $this->application = $application;
        $this->paths = $this->getTemplatePaths();

        // Set cache directory
        if (! is_dir($this->application->getDirectory('documentRoot') . self::FLUID_CACHE_DIRECTORY)) {
            mkdir($this->application->getDirectory('documentRoot') . self::FLUID_CACHE_DIRECTORY);
        }

        $this->setCache(
            new SimpleFileCache($this->application->getDirectory('documentRoot') . self::FLUID_CACHE_DIRECTORY)
        );
    }

    /**
     * @return TemplatePaths
     */
    public function getPaths()
    {
        return $this->paths;
    }
}
